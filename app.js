var names = ["Kowloon Tong", "Kwun Tong", "Kwai Fong", "Sai Wan Ho", "Shau Kei Wan", "Heng Fa Chuen", "Kowloon", "Airport", "Tai Koo", "Lok Fu", "Choi Hung", "Ngau Tau Kok", "Yau Tong", "Tiu Keng Leng", "Lai King", "Po Lam", "LOHAS Park", "Kowloon", "Sunny Bay", "Tung Chung", "Disneyland Resort", "Sha Tin", "Racecourse", "University", "Tai Po Market", "Tai Wo", "Fanling", "Sheung Shui", "Lok Ma Chau", "Lo Wu", "Che Kung Temple", "Sha Tin Wai", "Shek Mun", "Tai Shui Hang", "Wu Kai Sha", "Tsuen Wan West", "Kam Sheung Road", "Yuen Long", "Long Ping", "Tin Shui Wai", "Siu Hong", "Tuen Mun", "Tin Hau", "Sham Shui Po", "HKU", "Kennedy Town", "Sai Ying Pun", "Jordan", "Cheung Sha Wan", "Causeway Bay", "Quarry Bay", "Lai Chi Kok", "Chai Wan", "Mong Kok East", "Diamond Hill", "Hung Hom", "Ocean Park", "Lei Tung", "South Horizons", "Admiralty", "Kowloon Bay", "Central", "Hong Kong", "Hong Kong", "Austin", "Tsim Sha Tsui", "East Tsim Sha Tsui", "City One", "Fortress Hill", "North Point", "Hang Hau", "Heng On", "Whampoa", "Lam Tin", "Mei Foo", "Mong Kok", "Prince Edward", "Nam Cheong", "Shek Kip Mei", "Tseung Kwan O", "Wan Chai", "Wong Chuk Hang", "Wong Tai Sin", "Yau Ma Tei", "Asia World-Expo", "Sheung Wan", "Fo Tan", "Ma On Shan", "Ho Man Tin", "Kwai Hing", "Olympic", "Tai Wo Hau", "Tsuen Wan", "Tsing Yi", "Tsing Yi", "Tai Wai"];

var positions;
d3.text("./data/positions.txt", function(error, data) {
  positions = data.split('\n');
  positions = positions.map(function(row) {
    return row.split(' ').map(parseFloat);
  });
  positions.pop();
  
  var w = 720;
  var h = 560;
  var smin = 0.95 * Math.min(w, h);
  
  var svg = d3.select("#main")
			        .append("svg")
			        .attr("width", w)
			        .attr("height", h);
  
  var minx = d3.min(positions, function(d) { return d[0]; });
  var maxx = d3.max(positions, function(d) { return d[0]; });
  var miny = d3.min(positions, function(d) { return d[1]; });
  var maxy = d3.max(positions, function(d) { return d[1]; });
  var dmin = Math.min(maxx-minx, maxy-miny);
  
  var xscale = d3.scale.linear()
                       .domain([minx+(maxx-minx-dmin)/2, maxx-(maxx-minx-dmin)/2])
                       .range([(w-smin)/2, w-(w-smin)/2])
                       .nice();
  
  var yscale = d3.scale.linear()
                       .domain([miny+(maxy-miny-dmin)/2, maxy-(maxy-miny-dmin)/2])
                       .range([(h-smin)/2, h-(h-smin)/2])
                       .nice();
  
  svg.selectAll("circle")
     .data(positions)
     .enter()
     .append("circle")
     .attr("cx", function(d) {
       return xscale(d[0]);
     })
     .attr("cy", function(d) {
       return yscale(d[1]);
     })
     .attr("r", 2);
  
  svg.selectAll("text")
     .data(positions)
     .enter()
     .append("text")
     .text(function(d, i) {
       return names[i]
     })
     .attr("x", function(d) {
       return xscale(d[0]) + 2;
     })
     .attr("y", function(d) {
       return yscale(d[1]) - 2;
     })
     .attr("font-size", 8);
});


