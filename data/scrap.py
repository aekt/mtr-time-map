import os
import requests
import numpy as np
from sklearn import manifold

# example url:
# http://www.mtr.com.hk/share/customer/include/getdata.php?sid=75&eid=71

def getdist(sid, eid):
  php = 'http://www.mtr.com.hk/share/customer/include/getdata.php'
  url = php + '?sid=' + str(sid) + '&eid=' + str(eid)
  getdata = requests.get(url)
  ejt = getdata.json()['time']
  if ejt == '':
    ejt = '0'
  return int(ejt)


print('Downloading station names ...')
fcdata_json = requests.get('http://www.mtr.com.hk/st/data/fcdata_json.php')
stations = fcdata_json.json()['faresaver']['facilities']

n = len(stations)
indice = [int(s['STATION_ID']) for s in stations]

names = [[int(s['STATION_ID']), s['STATION_NAME_EN']] for s in stations]
names.insert(0, ['ID', 'NAME'])
np.savetxt('names.txt', names, fmt='%s', delimiters=', ')

print('Downloading station distance ...')
if os.path.isfile('distances.txt'):
  D = np.loadtxt('distances.txt', dtype=int)
else:
  D = np.zeros((n, n))
  for i in range(n):
    for j in range(i):
      D[i, j] = getdist(indice[i], indice[j])
    print('Node #%d: okay' % i)
  D = D + np.transpose(D)
  np.savetxt('distances.txt', D, fmt='%d')

print('Computing MDS ...')
mds = manifold.MDS()
pos = mds.fit_transform(D)
np.savetxt('positions.txt', pos, fmt='%f')

print('Done.')

